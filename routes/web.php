<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['web']],function (){
  Route::get('/', function () {
    return view('welcome');
  })->name('home');

  Route::post('/signup',[
    'uses'=>'UserController@postSignUp',
    'as'=>'signup'
  ]);
  Route::get('/dashboard',[
    'uses'=>'PostController@getDashboard',
    'as'=>'dashboard',
    'middleware'=>'auth'
  ]);

  Route::post('/signin',[
    'uses'=>'UserController@postSignIn',
    'as'=>'signin'
  ]);
  Route::post('/createpost',[
    'uses'=> 'PostController@postCreatePost',
    'as' =>'post.create'
  ]);
  Route::get('/homepage', function () {
      return view('homepage');
  })->middleware('auth');

  Route::get('/aboutKinkLink', function () {
      return view('aboutKinkLink');
  });
  Route::get('/welcome0', function () {
      return view('welcome0');
  })->middleware('auth');
  Route::get('/welcome1', function () {
      return view('welcome1');
  })->middleware('auth');
  Route::get('/welcome2', function () {
      return view('welcome2');
  })->middleware('auth');
  Route::get('/welcome3', function () {
      return view('welcome3');
  })->middleware('auth');
  Route::get('/welcome4', function () {
      return view('welcome4');
  })->middleware('auth');
  Route::get('/welcome5', function () {
      return view('welcome5');
  })->middleware('auth');
  Route::get('/welcome6', function () {
      return view('welcome6');
  })->middleware('auth');
  Route::get('/ModelCloud', function () {
      return view('ModelCloud');
  })->middleware('auth');
  // Route::get('/payment',[
  //   'uses'=>'PaymentController@getPayment',
  //   'as'=>'payment',
  //   'middleware'=>'auth'
  // ]);
  Route::get('/subscription', function () {
      return view('subscription');
  })->middleware('auth');

  Route::get('/model_dashboard', 'DropzoneFileUploadController@index');

  Route::post('store', 'DropzoneFileUploadController@uploadImages');

  Route::post('delete', 'DropzoneFileUploadController@deleteImage');
  });
