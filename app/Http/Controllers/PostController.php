<?php
namespace App\Http\Controllers;
use App\Post;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
// use App\User;
class PostController extends Controller
{
    public function getDashboard()
    {
      $posts =Post::all();
      return view('dashboard', [
        'posts'=>$posts
      ]);
    }

    public function postCreatePost(Request $request)
    {
      $this->validate($request, [
        'body'=>'required|max:150'
      ]);
      $post = new Post();
      $post->body = $request['body'];
      $message ='there was an error';
      if ($request->user()->posts()->save($post)){
        $message = 'POST SUCCESSFUL';

      }
      return redirect()->route('dashboard')->with(['message'=>$message]);
    }
}
