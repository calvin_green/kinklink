<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models;
class ModelController extends Controller
{
  public function getModelsDashboard()
  {
    return view('model_dashboard');
  }
    public function postModelSignUp(Request $request)
    {
      $this->validate($request,[
        'models_email'=> 'required|email|unique:models',
        'models_first_name'=>'required|max:25',
        'models_password'=>'required|min:4',
      ]);

      $email = $request['models_email'];
      $first_name = $request['models_first_name'];
      $password = bcrypt($request['models_password']);

      $user = new Models();
      $user->email = $email;
      $user->first_name = $first_name;
      $user->password = $password;

      $user->save();

      Auth::login($user);

      return redirect()->route('model_dashboard');

    }
    public function postModelSignIn(Request $request)
    {
      $this->validate($request, [
        'models_email'=>'required',
        'models_password'=>'required'
      ]);
      if (Auth::attempt(['models_email'=> $request['models_email'],'models_password'=>$request['models_password']])){
        return redirect()->route('model_dashboard');
      }
      return redirect()->back();

    }
}
